$(document).ready(function() 
{ 
    $('h3.section-title').each(function() { 
        var h = $(this).html(); 
        var index = h.indexOf(' '); 
        if(index == -1) { 
            index = h.length; 
        } 
        $(this).html('<span class="firstWord">' + h.substring(0, index) + '</span>' + h.substring(index, h.length)); 
    }); 
      $("#owl").owlCarousel({
 
      navigation : true, 
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true

  });

  $(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
      $('header').addClass("sticky");
    }
    else{
      $('header').removeClass("sticky");
    }

  });

   
  $(function() {

      $("<select />").appendTo(".main-nav");

     $(".main-nav a").each(function() {
          var el = $(this);
          $("<option />", {
              "value": el.attr("href"),
              "text": el.text()
          }).appendTo(".main-nav select");
      });

     $(".main-nav select").change(function() {
          window.location = $(this).find("option:selected").val();
      });
  });

  window.onscroll = function vverh() {
    document.getElementById('btn-up').style.display = (window.pageYOffset > '200' ? 'block' : 'none');
  }

});
